// Відповіді на питання:
// 1. Функції потрібні для того, щоб уникати дублювання коду, його зручного написання та читання.
// 2. Передача аргумента у функцію потрібна для того, щоб зайвий раз не створювати змінну, та можливості кожний раз
//    змінювати цей аргумент при виклику функції.
// 3. Return - оператор, який працює усередині функції задля того, щоб повертати результат, що був переданий у код.
//    Після написання return функція одразу зупиняє свою роботу.


function operationTo() {
    let num1 = parseFloat(prompt("Enter first number"))
    let num2 = parseFloat(prompt("Enter second number"))
    let operation = prompt("Enter math operation (+; -; *; /)")
    while (isNaN(num1) || isNaN(num2) || operation === '') {
        alert("Please enter numbers and math operation again")
        num1 = parseFloat(prompt("Enter first number"))
        num2 = parseFloat(prompt("Enter second number"))
        operation = prompt("Enter math operation (+; -; *; /)")
    }
    let result
    switch (operation) {
        case "+":
            result = num1 + num2
            break;
        case "-":
            result = num1 - num2
            break;
        case "*":
            result = num1 * num2
            break;
        case "/":
            if (num2 === 0) {
                console.log("Division by zero is not allowed.")
                return;
            }
            result = num1 / num2
            break;
        default:
            console.log("error")
            return;
    }
    console.log(result)
}

operationTo()
